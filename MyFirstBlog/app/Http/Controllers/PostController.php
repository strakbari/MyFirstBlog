<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    private $user ;

    public function __construct()
    {
        $this->user=Auth::user();
    }

    public function create(Request $request)
    {
        if($this->user->can_post())
        {
            return view('posts.create');
        }
        else
        {
            return redirect('/')->withErrors('You have not sufficient permissions for writing post');
        }
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|string|max:100',
            'body' => 'required|string',

        ]);

        $user=$this->user;
        $flag=Post::where('slug',str_slug(Input::get('title')))->first();
        if ($flag){
            return redirect('newPost')->withErrors("already exists...");
        }
        $active=0 ;
        $message= null ;

        if (Input::has('save')){

            $message="post saved successfully!!";

        }
        else{
            $active=1;
            $message="post published successfully!!" ;
        }

        $user->posts()->create([

                'title'=>Input::get('title'),
                'body'=>Input::get('body'),
                'slug'=>str_slug(Input::get('title')),
                'active'=>$active
        ]);

        return $message;

     }

     public function userAllPosts(Request $request){

        $user=Auth::user();
        $posts=$user->posts()->paginate(5);
        $title=$user->name;

         return view('home')->with('posts',$posts)->withTitle($title);


     }

    public function edit(Request $request,$slug){

        $user=$this->user;
        $post = Post::where('slug',$slug)->first();
        if($post && ($user->id == $post->user_id ||
                $user->is_admin()))
            return view('posts.edit')->with('post',$post);
        else
        {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }

    }


    public function update(Request $request)
    {
        //
        $post_id = $request->input('post_id');
        $post = Post::find($post_id);
        if($post && ($post->user_id == $this->user->id || $this->user->is_admin()))
        {
            $title = $request->input('title');
            $slug = str_slug($title);
            $duplicate = Post::where('slug',$slug)->first();
            if($duplicate)
            {
                if($duplicate->id != $post_id)
                {
                    return redirect('edit/'.$post->slug)->withErrors('Title already exists.')->withInput();
                }
                else
                {
                    $post->slug = $slug;
                }
            }

            $post->title = $title;
            $post->body = $request->input('body');

            if($request->has('save'))
            {
                $post->active = 0;
                $message = 'Post saved successfully';
                $landing = 'edit/'.$post->slug;
            }
            else {
                $post->active = 1;
                $message = 'Post updated successfully';
                $landing = $post->slug;
            }
            $post->save();
//            dd($landing);
            return redirect($landing)->withMessage($message);
        }
        else
        {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }

    public function destroy(Request $request, $id)
    {
        //
        $post = Post::find($id);
        if($post && ($post->user_id == $this->user->id || $this->user->is_admin()))
        {
            $post->delete();
            $data['message'] = 'Post deleted Successfully';
        }
        else
        {
            $data['errors'] = 'Invalid Operation. You have not sufficient permissions';
        }
        dd($data);
        return redirect('/')->with($data);
    }

    public function userDrafts(request $request){

        $user=$this->user ;
        $posts= Post::where('user_id',$user->id)->where('active','0')->orderBy('created_at','desc')->paginate(5);
        $title=$user->name ;
        return view('home')->withPosts($posts)->withTitle($title);

    }

    public function show($slug)
    {
        $post = Post::where('slug',$slug)->first();

        if($post)
        {
            if($post->active == false)
//                dd('salam');
                return redirect('/')->withErrors('requested page not found');
            $comments = $post->comments;
        }
        else
        {
            return redirect('/')->withErrors('requested page not found');
        }
        return view('posts.show')->withPost($post)->withComments($comments);
    }
}
