<?php
/**
 * Created by PhpStorm.
 * User: Farzane Fakouri
 * Date: 8/16/2017
 * Time: 3:26 AM
 */

namespace App\Http\Controllers;


use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{


    private $user ;

    public function __construct()
    {
        $this->user=Auth::user();
    }

    public function store(Request $request)
    {
        //on_post, from_user, body
        $input['user_id'] = $this->user->id;
        $input['post_id'] = $request->input('post_id');
        $input['body'] = $request->input('body');
        $slug = $request->input('slug');
//        $slug=str_slug(Input::get('title'));
        Comment::create( $input );

        return redirect($slug)->with('message', 'Comment published');
    }

//    public function store(Request $request)
//    {
//
//        $post=$this->post;
//
//        $message= null ;
//
//        if (Input::has('post_comment')){
//
//            $message="Comment posted successfully!!";
//
//        }
//        else{
//            $active=1;
//            $message="post published successfully!!" ;
//        }
//
//        $post->comments()->create([
//
//            'user_id'=>Input::get('user_id'),
//            'post_id'=>Input::get('post_id'),
//            'body'=>Input::get('body')
//
//        ]);




//
//    private $user ;
//
//    public function __construct()
//    {
//        $this->user=Auth::user();
//    }
//
//    public function create(Request $request)
//    {
//        if($this->user->can_post())
//        {
//            return view('posts.create');
//        }
//        else
//        {
//            return redirect('/')->withErrors('You have not sufficient permissions for writing post');
//        }
//    }
//    public function store(Request $request)
//    {
//
//        $this->validate($request, [
//            'title' => 'required|string|max:100',
//            'body' => 'required|string',
//
//        ]);
//
//        $user=$this->user;
//        $flag=Post::where('slug',str_slug(Input::get('title')))->first();
//        if ($flag){
//            return redirect('newPost')->withErrors("already exists...");
//        }
//        $active=0 ;
//        $message= null ;
//
//        if (Input::has('save')){
//
//            $message="post saved successfully!!";
//
//        }
//        else{
//            $active=1;
//            $message="post published successfully!!" ;
//        }
//
//        $user->posts()->create([
//
//            'title'=>Input::get('title'),
//            'body'=>Input::get('body'),
//            'slug'=>str_slug(Input::get('title')),
//            'active'=>$active
//        ]);
//
//        return $message;
//
//    }
}