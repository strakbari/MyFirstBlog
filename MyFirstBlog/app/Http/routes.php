<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', function () {
    return view('layouts.main');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=>['web']],function()
{
    Route::get('newPost','PostController@create');
    Route::post('newPost','PostController@store');
    Route::get('userAllPosts','PostController@userAllPosts');
    Route::get('edit/{slug}','PostController@edit');
    Route::post('update','PostController@update');
    Route::get('delete/{id}','PostController@destroy');
    Route::get('myDrafts','PostController@userDrafts');
    Route::get('/{slug}',['as' => 'post', 'uses' => 'PostController@show'])->where('slug', '[A-Za-z0-9-_]+');
    Route::post('comment/add','CommentController@store');





});